import "./scss/main.scss";
import Swiper from "swiper";

const swiper = document.querySelector(".slider-container");
const swiperSlider = new Swiper(swiper, {
  slidesPerView: "auto",
  centredSlides: true,
  loop: true,
  spaceBetween: 105,
  watchOverflow: false,
});
